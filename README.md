Mobile 2D scanning platform
Design assumptions:
- the vehicle will run on flat ground without any obstacles
- avoid obstacles such as walls
- protection against falling down the stairs
- moving in a way that allows you to scan the entire room
- Sufficient power to scan several rooms (2 cells 3,6V)
- data visualization on a computer
- android phone control like RC car / robot auto move
