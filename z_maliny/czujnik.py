from rplidar import RPLidar
import math
import wysylanie


def start_lidar():
    lidar = RPLidar('/dev/ttyUSB0')
    lidar.stop_motor()
    lidar.stop()
    lidar.disconnect()
    lidar = RPLidar('/dev/ttyUSB0')
    return lidar


def create_scan(lidar,snap_id):
    for i, scan in enumerate(lidar.iter_scans(100,5)):
        for z, lista in enumerate(scan):
            wysylanie.wyslij(math.cos(math.radians(scan[z][1]))*scan[z][2], math.sin(math.radians(scan[z][1]))*scan[z][2],snap_id)
            if i > 2:
                break
            
            
def stop_lidar(lidar):
    lidar.stop()
    lidar.stop_motor()
    lidar.disconnect()

