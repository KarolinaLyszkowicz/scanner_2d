import RPi.GPIO as GPIO
from time import sleep


def konfiguracja():
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)

    GPIO.setup(16,GPIO.OUT)
    GPIO.setup(20,GPIO.OUT)
    GPIO.setup(21,GPIO.OUT)

    p = GPIO.PWM(16,50)
    p.start(0)
 
    GPIO.setup(13,GPIO.OUT)
    GPIO.setup(19,GPIO.OUT)
    GPIO.setup(26,GPIO.OUT)

    l = GPIO.PWM(26,50)
    l.start(0)
    return p,l


def prawy(en,p):

    if en>=0:
        if en>100:
            print("za duza predkosc silnika")
        GPIO.output(21,GPIO.LOW)
        GPIO.output(20,GPIO.HIGH)
        p.ChangeDutyCycle(en)

    else:
        if en<-100:
            print("za duza predkosc silnika")
        GPIO.output(21,GPIO.HIGH)
        GPIO.output(20,GPIO.LOW)
        p.ChangeDutyCycle(abs(en))


def lewy(en2,l):

    if en2>=0:
        if en2>100:
            print("za duza predkosc silnika")
        GPIO.output(13,GPIO.HIGH)
        GPIO.output(19,GPIO.LOW)
        l.ChangeDutyCycle(en2)
    else:
        if en2<-100:
            print("za duza predkosc silnika")
        GPIO.output(13,GPIO.LOW)
        GPIO.output(19,GPIO.HIGH)
        l.ChangeDutyCycle(abs(en2))
        
def koniec(p,l):
    p.stop()
    l.stop()
    GPIO.cleanup()

"""
while True:
    GPIO.output(21,GPIO.HIGH)
    sleep(0.5)
    GPIO.output(21,GPIO.LOW)
    sleep(0.5)
    """

"""
p = GPIO.PWM(21,50)
p.start(0)
for i in range(0,101,5):
    p.ChangeDutyCycle(i)
    sleep(0.1)
    print(i)
    
for i in range(100,-1,-5):
    p.ChangeDutyCycle(i)
    sleep(0.1)
    print(i)
    """