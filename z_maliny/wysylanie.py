import requests
import json

def wyslij(coordinate_x, coordinate_y, snap_id):
    headers = {'content-type': 'application/json'}
    body = json.dumps({"coordinate_x": coordinate_x, "coordinate_y": coordinate_y, "snap_id": snap_id})
    headers2 = {
        'content-type': 'application/json',
        'content-length': f"\"{len(body)}\""
        }

    url = 'https://janos.pythonanywhere.com/pointadd'
    odp = requests.post(url, data=body, headers=headers2)
    #print(odp)
    