from time import sleep
import lib_silniki as silniki
import czujnik
import requests
import json


def move_on_segment(p,l,length_cm: int = 0) -> None:
    silniki.lewy(30,l)
    silniki.prawy(30,p)
    sleep(2)
    silniki.lewy(0,l)
    silniki.prawy(0,p)
    sleep(0.5)


def take_a_snap(ccoordinate_x, ccoordinate_y, nr: int = 1, scan_id: int = 2):
    headers = {'content-type': 'application/json'}
    body = json.dumps({"camera_coordinate_x": ccoordinate_x,
                       "camera_coordinate_y": ccoordinate_y,
                       "number": nr, "scan_id": scan_id})
    headers2 = {
        'content-type': 'application/json',
        'content-length': f"\"{len(body)}\""
        }

    url = 'https://janos.pythonanywhere.com/snapadd'
    odp = requests.post(url, data=body, headers=headers2)
    #print(odp)


def turn_left(p,l) -> None:
    silniki.lewy(-39,l)
    silniki.prawy(39,p)
    sleep(2)
    silniki.lewy(0,l)
    silniki.prawy(0,p)
    sleep(0.5)


def go_on_rectangle(p,l,lidar,segment_length_cm: int = 10,
                    segments_amount_x: int = 2,
                    segments_amount_y: int = 1
                    ) -> None:
    #####
    snap_id = 10
    cc_x = 0
    cc_y = 0
    first_turn = True
    for j in range(2):
        for i in range(segments_amount_x):
            move_on_segment(p,l,segment_length_cm)
            if first_turn:
                cc_x = cc_x + 1
            else:
                cc_x = cc_x - 1
            take_a_snap(cc_x, cc_y, 1, 2)
            czujnik.create_scan(lidar, snap_id)
            snap_id = snap_id + 1

        turn_left(p,l)

        for i in range(segments_amount_y):
            move_on_segment(p,l,segment_length_cm)
            if first_turn:
                cc_y = cc_y + 1
            else:
                cc_y = cc_y - 1
            take_a_snap(cc_x, cc_y, 1, 2)
            czujnik.create_scan(lidar, snap_id)
            snap_id = snap_id + 1

        turn_left(p,l)
    silniki.koniec(p,l)


if __name__ == "__main__":
    
    
    p,l = silniki.konfiguracja()
    lidar = czujnik.start_lidar()
    #go_on_rectangle(p,l,lidar)
    silniki.koniec(p,l)
    czujnik.stop_lidar(lidar)
 