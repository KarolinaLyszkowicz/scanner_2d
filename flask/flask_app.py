from flask import Flask, redirect, render_template, request, jsonify, abort, flash
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import desc ,asc
from flask_marshmallow import Marshmallow
from marshmallow import Schema, fields, pprint
from datetime import datetime, timedelta
import os
from os.path import isfile, join
from os import listdir
import json
from io import StringIO
from werkzeug.wrappers import Response
import itertools
import random
import string


app = Flask(__name__)
app.secret_key = 'development key'

SQLALCHEMY_DATABASE_URI = "mysql+mysqlconnector://{username}:{password}@{hostname}/{databasename}".format(
    username="janos",
    password="prostehaslo123", # database passowrd hidden
    hostname="janos.mysql.pythonanywhere-services.com",
    databasename="janos$IPZ",
)
app.config["SQLALCHEMY_DATABASE_URI"] = SQLALCHEMY_DATABASE_URI
app.config["SQLALCHEMY_POOL_RECYCLE"] = 299 # connection timeouts
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False # no warning disruptions

db = SQLAlchemy(app)
ma = Marshmallow(app)


################################################################################
#
class Scan(db.Model):

    __tablename__="Scan"
    id = db.Column(db.Integer, primary_key=True)
    name=db.Column(db.String(4096))
    date=db.Column(db.DATETIME)


    def __init__(self, name, date):
        self.name = name
        self.date = date

class ScanSchema(ma.Schema):
    class Meta:
        fields =('id','name','date')



class Snaps(db.Model):
    __tablename__="Snap"
    id = db.Column(db.Integer, primary_key=True)
    scan_id=db.Column(db.Integer)
    camera_coordinate_x = db.Column(db.Float)
    camera_coordinate_y = db.Column(db.Float)
    number = db.Column(db.Integer)

    def __init__(self,scan_id,camera_coordinate_x, camera_coordinate_y, number):
        self.scan_id=scan_id
        self.camera_coordinate_x=camera_coordinate_x
        self.camera_coordinate_y=camera_coordinate_y
        self.number=number

class SnapsSchema(ma.Schema):
    class Meta:
        fields=('id','scan_id','camera_coordinate_x', 'camera_coordinate_y', 'number')

class Point(db.Model):
    __tablename__="Point"
    id = db.Column(db.Integer, primary_key=True)
    snap_id=db.Column(db.Integer)
    coordinate_x=db.Column(db.Float)
    coordinate_y = db.Column(db.Float)


    def __init__(self, snap_id,coordinate_x, coordinate_y):
        self.snap_id=snap_id
        self.coordinate_x=coordinate_x
        self.coordinate_y=coordinate_y

class PointSchema(ma.Schema):
    class Meta:
        fields =('id','snap_id','coordinate_x','coordinate_y')


#
################################################################################
################################################################################
#
scan_schema=ScanSchema()
scans_schema=ScanSchema(many=True)

snap_schema=SnapsSchema()
snaps_schema=SnapsSchema(many=True)

point_schema=PointSchema()
points_schema=PointSchema(many=True)


#
#####################################################

@app.route('/z')
def hello_world():
    return 'Hello from Flask!'

################################################################################
#
@app.route("/scan", methods=["GET"])
def get_scan():
    all_scans=Scan.query.all()
    result=scans_schema.dump(all_scans)
    return jsonify(result)


@app.route("/scan/<id>", methods=["GET"])
def get_scanid(id):
    scan = Scan.query.get(id)
    result = scan_schema.dump(scan)
    return jsonify(result)


@app.route("/scanadd",methods=["POST"])
def add_scan():
    name=request.json["name"]
    new_scan=Scan(name,datetime.now())
    db.session.add(new_scan)
    db.session.commit()
    scan=Scan.query.get(new_scan.id)
    return scan_schema.jsonify(scan)

################################################################################

@app.route("/snap", methods=["GET"])
def get_snap():
    all_snaps=Snaps.query.all()
    result=snaps_schema.dump(all_snaps)
    return jsonify(result)


@app.route("/snap/<id>", methods=["GET"])
def get_snapid(id):
    snap = Snaps.query.get(id)
    result = snap_schema.dump(snap)
    return jsonify(result)


@app.route("/snapadd", methods=["POST"])
def add_snap():
    camera_coordinate_x=request.json["camera_coordinate_x"]
    camera_coordinate_y=request.json["camera_coordinate_y"]
    number=request.json["number"]
    scan_id=request.json["scan_id"]
    new_snap = Snaps(scan_id,camera_coordinate_x, camera_coordinate_y, number)
    db.session.add(new_snap)
    db.session.commit()
    snap = Snaps.query.get(new_snap.id)
    return snap_schema.jsonify(snap)

################################################################################

@app.route("/point", methods=["GET"])
def get_point():
    all_points=Point.query.all()
    result=points_schema.dump(all_points)
    return jsonify(result)

@app.route("/point/<id>", methods=["GET"])
def get_pointid(id):
    point = Point.query.get(id)
    result = point_schema.dump(point)
    return jsonify(result)

@app.route("/pointadd", methods=["POST"])
def add_point():
    cx=request.json["coordinate_x"]
    cy=request.json["coordinate_y"]
    snap_id=request.json["snap_id"]
    new_point =Point(snap_id,cx, cy)
    db.session.add(new_point)
    db.session.commit()
    point = Point.query.get(new_point.id)
    return point_schema.jsonify(point)
################################################################################












