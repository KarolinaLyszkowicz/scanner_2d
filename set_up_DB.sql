CREATE TABLE `Scan` (
  `Id` int PRIMARY KEY AUTO_INCREMENT,
  `Name` varchar(255),
  `Date` datetime
);

CREATE TABLE `Snap` (
  `Id` int PRIMARY KEY AUTO_INCREMENT,
  `Scan_Id` int,
  `Camera_Coordinate_X` double,
  `Camera_Coordinate_Y` double,
  `Number` int
);

CREATE TABLE `Point` (
  `Id` int PRIMARY KEY AUTO_INCREMENT,
  `Snap_Id` int,
  `Coordinate_X` double,
  `Coordinate_Y` double
);

ALTER TABLE `Snap` ADD FOREIGN KEY (`Scan_Id`) REFERENCES `Scan` (`Id`);

ALTER TABLE `Point` ADD FOREIGN KEY (`Snap_Id`) REFERENCES `Snap` (`Id`);
